//Activity S24


//Show users Email and isAdmin field only with 'y' in 1stname or lastname
db.users.find({
	$or: [
	{
		"lastName": {$regex: 'y', $options: '$i'}
	},
	{
		"firstName": {$regex: 'y', $options: '$i'}
	}]
},
{
	"_id": 0, "email": 1, "isAdmin": 1
})


//Show users Email and isAdmin field only with 'e' in 1stname and isAdmin
db.users.find({
	"isAdmin": true,
	$and: [
	{
		"firstName": {$regex: 'e', $options: '$i'}
	}]
},
{
	"_id": 0, "email": 1, "isAdmin": 1
})


//Find Products with 'x' in name price gte 50000
db.products.find({
	$and: [
	{
		"price": {$gte: 50000}
	},
	{
		"name": {$regex: 'x', $options: '$i'}
	}]
})


//Update products price lt 2000 to inactive status
db.products.updateMany(
	{
		"price" : {$lt: 2000}
	},
	{
		$set: {"isActive" : false}
	})


//Delete all products with price gt 20000
db.products.deleteMany(
	{
		"price" : {$gt: 20000}
	})








