/*
Query Operators and Field Projection




*/

db.products.insertMany([
		{
			"name": "IPhone X",
			"price": 30000,
			"isActive": true
		},
		{
			"name": "Samsung Galaxy S21",
			"price": 51000,
			"isActive": true
		},
		{
			"name": "Razer  Blackshark V2X",
			"price": 2000,
			"isActive": false
		},
		{
			"name": "RAKK Gaming Mouse",
			"price": 1000,
			"isActive": true
		},
		{
			"name": "Razer Mechanical Keyboard",
			"price": 4000,
			"isActive": true
		}

	])

/*
Query operators

allow users for more flexible querying in MongoDB
Instead of being able to find/ search for documents with exact values
we use operators to define conditions

$gt - greater than
$lt - less than
$gte - greater than or equal
$lte - less than or equal
*/

db.products.find({"price" : {$gt: 3000}})

db.products.find({"price" : {$gte: 30000}})

/*
In robo data manipulation, we can include many codes in the shell, and highlight the specific command we want to execute.
*/

db.products.find({"price" : {$lt: 4000}})

db.products.find({"price" : {$lte: 2800}})

/*
Query Operators  can be used for updating and deleting

*/

db.products.updateMany(
	{
		"price" : {$gte: 30000}},
	{
		$set: {"isActive": false}
	})

db.users.insertMany([
		{
			"firstName": "Mary Jane",
			"lastName": "Watson",
			"email": "mjtiger@gmail.com"
			"password": "tigerjackpot15",
			"isAdmin": false
		},
		{
			"firstName": "Gwen",
			"lastName": "Stacy",
			"email": "stacyTech@gmail.com"
			"password": "stacyTech1991",
			"isAdmin": true
		},
		{
			"firstName": "Peter",
			"lastName": "Parker",
			"email": "peterWebDev@gmail.com"
			"password": "webDeveloperPeter",
			"isAdmin": true
		},
		{
			"firstName": "Jonah",
			"lastName": "Jameson",
			"email": "jjjameson@gmail.com"
			"password": "spideyisamenace",
			"isAdmin": false
		},
		{
			"firstName": "Otto",
			"lastName": "Octavius",
			"email": "ottoOctopi@gmail.com"
			"password": "docOck15",
			"isAdmin": true
		}
	])

/*
Regex
used to find documents in which it will match the characters  or patterns the characters were looking for.
*/

db.users.find({
	"firstname": {$regex: '0'}
});

//regex for Case sensitive, technically will return data with first initial '<char>'
db.users.find({
	"firstname": {$regex: 'W'}
})

//options - case insensitive so technically returns data which includes '<char>' in the specific criteria

db.users.find({
	"lastName": {$regex: 'o', $options: '$i'}
})

db.users.find({
	"email": {$regex: 'web', $options: '$i'}
})



db.products.find({
	"name": {$regex: 'razer', $options: '$i'}
})
db.products.find({
	"name": {$regex: 'rakk', $options: '$i'}
})

// $or $and
/*
$or allow us to have a logical operation wherein we can look  or find for a document which can satisfy at least one of our condition
*/

db.products.find({
	$or: [
		{"name": {$regex: 'x', $options: '$i'}},
		{"price": {$lte: 10000}}
	]
})

db.products.find({
	$or: [
		{"name": {$regex: 'x', $options: '$i'}},
		{"price": 30000}
	]
})

db.users.find({
	$or: [
		{"firstName": {$regex: 'a', $options: '$i'}},
		{"isAdmin": true}
	]
})

db.users.find({
	$or: [
		{"lastName": {$regex: 'w', $options: '$i'}},
		{"isAdmin": false}
	]
})

/*
And operator allows us to have a logical operation wherein we can look for a document which can satisfy both the condition
*/

db.products.find({
	$and: [
	{
		"name": {$regex: 'razer ', {$options: '$i'}},
	{
		"price": {$gte: 3000}
	}
	}]
})



db.users.find({
	$and: [
	{
		"lastName": {$regex: 'w', {$options: '$i'}},
	{
		"isAdmin": false
	}
	}]
})

db.users.find({
	$or: [
	{
		"lastName": {$regex: 'y', $options: '$i'}
	},
	{
		"firstName": {$regex: 'y', $options: '$i'}
	}]
},
{
	"_id": 0, "email": 1, "isAdmin": 1
})

//Field Projection
//command hides the specific field
db.users.find({}, {"_id": 0, "password": 0})

/*
find can have two arguments:
db.<collection>.find({query}, {"field" : 0/1})
*/


db.users.find({"isAdmin": false}, {"_id": 0, "email": 1})

db.products.find(
{
	"price": {$gte: 10000}
},
{
	"_id": 0, "name": 1, "price": 1
})






